# yamllint disable
# License: Commons Clause License Condition v1.0[LGPL-2.1-only]
# yamllint enable
---
rules:
  - id: java_crypto_rule-HTTPUrlConnectionHTTPRequest
    message: |
      Detected an HTTP request sent via HttpURLConnection or URLConnection.
      This could lead to sensitive information being sent over an insecure 
      channel, as HTTP does not encrypt data. Transmitting data over HTTP 
      exposes it to potential interception by attackers, risking data 
      integrity and confidentiality. Using HTTP for transmitting sensitive 
      data such as passwords, personal information, or financial details can 
      lead to information disclosure.

      To mitigate the issue, switch to HTTPS to ensure all data transmitted 
      is securely encrypted. This helps protect against eavesdropping and 
      man-in-the-middle attacks. Modify the URL in your code from HTTP to 
      HTTPS and ensure the server supports HTTPS.

      Secure Code Example:
      ```
      private static void safe() {
          try {
              URL url = new URL("https://example.com/api/data"); // Changed to HTTPS
              HttpURLConnection con = (HttpURLConnection) url.openConnection();
              con.setRequestMethod("GET");

              int status = con.getResponseCode();
              if (status == HttpURLConnection.HTTP_OK) { 
                  BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
                  String inputLine;
                  StringBuilder response = new StringBuilder();
                  while ((inputLine = in.readLine()) != null) {
                      response.append(inputLine);
                  }
                  in.close();
                  System.out.println("Response: " + response.toString());
              } else {
                  System.out.println("HTTP error code: " + status);
              }
              con.disconnect();
          } catch (Exception e) {
              e.printStackTrace();
          }
      }
      ```
    severity: "WARNING"
    metadata:
      likelihood: "MEDIUM"
      impact: "MEDIUM"
      confidence: "MEDIUM"
      shortDescription: "Cleartext transmission of sensitive information"
      category: "security"
      cwe: "CWE-319"
      owasp:
        - "A3:2017-Sensitive Data Exposure"
        - "A02:2021-Cryptographic Failures"
      security-severity: "MEDIUM"
      references:
        - "https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/net/URLConnection.html"
        - "https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/net/URL.html#openConnection()"
      subcategory:
        - "vuln"
      technology:
        - "java"
      vulnerability: "Insecure Transport"
      license: "Commons Clause License Condition v1.0[LGPL-2.1-only]"
      vulnerability_class:
        - "Mishandled Sensitive Information"
    languages:
      - "java"
    patterns:
      - pattern: |
          "=~/[Hh][Tt][Tt][Pp]://.*/"
      - pattern-either:
          - pattern-inside: |
              URL $URL = new URL ("=~/[Hh][Tt][Tt][Pp]://.*/", ...);
              ...
              $CON = (HttpURLConnection) $URL.openConnection(...);
              ...
              $CON.$FUNC(...);
          - pattern-inside: |
              URL $URL = new URL ("=~/[Hh][Tt][Tt][Pp]://.*/", ...);
              ...
              $CON = $URL.openConnection(...);
              ...
              $CON.$FUNC(...);
