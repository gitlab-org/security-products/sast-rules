# yamllint disable
# License: Commons Clause License Condition v1.0[LGPL-2.1-only]
# https://github.com/semgrep/semgrep-rules/blob/release/problem-based-packs/insecure-transport/java-spring/spring-ftp-request.yaml
# yamllint enable
---
rules:
  - id: java_crypto_rule-SpringFTPRequest
    languages:
      - "java"
    mode: taint
    pattern-sources:
      - pattern: |
          "=~/^ftp://.+/i"
    pattern-sinks:
      - patterns:
          - pattern: |
              (org.springframework.integration.ftp.session.DefaultFtpSessionFactory
              $SF).setHost($URL);
          - focus-metavariable: $URL
    message: |
      This pattern detects configurations where the Spring Integration FTP plugin 
      is used to set up connections to FTP servers. FTP is an insecure protocol 
      that transmits data, including potentially sensitive information, in plaintext. 
      This can expose personal identifiable information (PII) or other sensitive data 
      to interception by attackers during transmission. 
      
      To mitigate the vulnerability, switch to a secure protocol such as SFTP or FTPS 
      that encrypts the connection to prevent data exposure. Ensure that any method 
      used to set the host for an FTP session does not use plaintext FTP. 

      Secure Code Example:
      ```
      public SessionFactory<FTPFile> safe(FtpSessionFactoryProperties properties) {
        DefaultFtpSessionFactory ftpSessionFactory = new DefaultFtpSessionFactory();
        ftpSessionFactory.setHost("sftp://example.com");
        ftpSessionFactory.setPort(properties.getPort());
        ftpSessionFactory.setUsername(properties.getUsername());
        ftpSessionFactory.setPassword(properties.getPassword());
        ftpSessionFactory.setClientMode(properties.getClientMode().getMode());
        return ftpSessionFactory;
      }
      ```
    severity: "WARNING"
    metadata:
      likelihood: "MEDIUM"
      impact: "MEDIUM"
      confidence: "MEDIUM"
      cwe: "CWE-319"
      category: "security"
      shortDescription: "Cleartext transmission of sensitive information"
      owasp:
        - "A3:2017-Sensitive Data Exposure"
        - "A02:2021-Cryptographic Failures"
      security-severity: "MEDIUM"
      references:
        - "https://docs.spring.io/spring-integration/api/org/springframework/integration/ftp/session/AbstractFtpSessionFactory.html#setClientMode-int-"
      subcategory:
        - "vuln"
      technology:
        - "spring"
      vulnerability: "Insecure Transport"
      license: "Commons Clause License Condition v1.0[LGPL-2.1-only]"
      vulnerability_class:
        - "Mishandled Sensitive Information"

