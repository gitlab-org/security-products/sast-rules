# yamllint disable
# License: Commons Clause License Condition v1.0[LGPL-2.1-only]
# https://github.com/semgrep/semgrep-rules/blob/release/problem-based-packs/insecure-transport/java-stdlib/unirest-http-request.yaml
# yamllint enable
---
rules:
  - id: java_crypto_rule-UnirestHTTPRequest
    languages:
      - "java"
    patterns:
      - pattern: |
          Unirest.$METHOD("=~/[hH][tT][tT][pP]://.*/")
    message: |
      This application uses the Unirest library to send
      network requests to URLs starting with 'http://'. Communicating over HTTP
      is considered insecure because it does not encrypt traffic with TLS
      (Transport Layer Security), exposing data to potential interception or
      manipulation by attackers.

      To mitigate the issue, modify the request URL to begin with 'https://' 
      instead of 'http://'. Using HTTPS ensures that the data is encrypted and 
      secure during transmission. Review all instances where HTTP is used and 
      update them to use HTTPS to prevent security risks.

      Secure Code Example:
      ```
      import kong.unirest.core.Unirest;

      public void safe() {
        Unirest.get("https://httpbin.org")
            .queryString("fruit", "apple")
            .queryString("droid", "R2D2")
            .asString();
      }
      ```
    severity: "WARNING"
    metadata:
      likelihood: "MEDIUM"
      impact: "MEDIUM"
      confidence: "MEDIUM"
      category: "security"
      cwe: "CWE-319"
      shortDescription: "Cleartext transmission of sensitive information"
      owasp:
        - "A3:2017-Sensitive Data Exposure"
        - "A02:2021-Cryptographic Failures"
      security-severity: "MEDIUM"
      references:
        - "https://kong.github.io/unirest-java/#requests"
      subcategory:
        - "vuln"
      technology:
        - "unirest"
      vulnerability: "Insecure Transport"
      license: "Commons Clause License Condition v1.0[LGPL-2.1-only]"
      vulnerability_class:
        - "Mishandled Sensitive Information"
