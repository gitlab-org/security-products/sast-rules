# yamllint disable
# License: Commons Clause License Condition v1.0[LGPL-2.1-only]
# https://github.com/semgrep/semgrep-rules/blob/release/java/lang/security/audit/xxe/saxparserfactory-disallow-doctype-decl-missing.yaml
# yamllint enable
---
rules:
  - id: java_xxe_rule-SAXParserFactoryDisallowDoctypeDeclMissing
    severity: "WARNING"
    languages:
      - "java"
    metadata:
      cwe: "CWE-611"
      shortDescription: "Improper restriction of XML external entity reference"
      owasp:
        - "A4:2017-XML External Entities (XXE)"
        - "A05:2021-Security Misconfiguration"
      security-severity: "MEDIUM"
      references:
        - "https://semgrep.dev/blog/2022/xml-security-in-java"
        - "https://semgrep.dev/docs/cheat-sheets/java-xxe/"
        - "https://blog.sonarsource.com/secure-xml-processor"
        - "https://xerces.apache.org/xerces2-j/features.html"
      category: "security"
      technology:
        - "java"
        - "xml"
      cwe2022-top25: "true"
      cwe2021-top25: "true"
      license: "Commons Clause License Condition v1.0[LGPL-2.1-only]"
      vulnerability_class:
        - "XML Injection"
    message: |
      DOCTYPE declarations are enabled for this SAXParserFactory. Enabling DOCTYPE 
      declarations without proper restrictions can make your application vulnerable to 
      XML External Entity (XXE) attacks. 
      In an XXE attack, an attacker can exploit the processing of external entity 
      references within an XML document to access internal files, conduct 
      denial-of-service attacks, or SSRF (Server Side Request Forgery), potentially 
      leading to sensitive information disclosure or system compromise.

      To mitigate this vulnerability, disable DOCTYPE declarations by setting the
      feature `http://apache.org/xml/features/disallow-doctype-decl` to true.
      Alternatively, allow DOCTYPE declarations and only prohibit external
      entities declarations. This can be done by setting the features
      `http://xml.org/sax/features/external-general-entities` and
      `http://xml.org/sax/features/external-parameter-entities` to false. 
      NOTE: The previous links are not meant to be clicked. They are the literal
      config key values that are supposed to be used to disable these features.

      Secure Code Example (You can do either of the following):
      ```
      public void GoodSAXParserFactory() throws  ParserConfigurationException {
        SAXParserFactory spf = SAXParserFactory.newInstance();
        spf.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
        spf.newSAXParser();
      }

      public void GoodSAXParserFactory2() throws  ParserConfigurationException {
        SAXParserFactory spf = SAXParserFactory.newInstance();
        spf.setFeature("http://xml.org/sax/features/external-parameter-entities", false);
        spf.setFeature("http://xml.org/sax/features/external-general-entities", false);
        spf.newSAXParser();
      }      
      ```
      For more information, see
      https://semgrep.dev/docs/cheat-sheets/java-xxe/#3a-documentbuilderfactory.
    mode: taint
    pattern-sources:
      - by-side-effect: true
        patterns:
          - pattern-either:
              - pattern: |
                  $FACTORY = SAXParserFactory.newInstance();
              - patterns:
                  - pattern: $FACTORY
                  - pattern-inside: |
                      class $C {
                        ...
                        $V $FACTORY = SAXParserFactory.newInstance();
                        ...
                      }
                  - pattern-not-inside: >
                      class $C {
                        ...
                        $V $FACTORY = SAXParserFactory.newInstance();
                        static {
                          ...
                          $FACTORY.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
                          ...
                        }
                        ...
                      }
                  - pattern-not-inside: >
                      class $C {
                        ...
                        $V $FACTORY = SAXParserFactory.newInstance();
                        static {
                          ...
                          $FACTORY.setFeature("http://xml.org/sax/features/external-parameter-entities", false);
                          ...
                          $FACTORY.setFeature("http://xml.org/sax/features/external-general-entities", false);
                          ...
                        }
                        ...
                      }
                  - pattern-not-inside: >
                      class $C {
                        ...
                        $V $FACTORY = SAXParserFactory.newInstance();
                        static {
                          ...
                          $FACTORY.setFeature("http://xml.org/sax/features/external-general-entities", false);
                          ...
                          $FACTORY.setFeature("http://xml.org/sax/features/external-parameter-entities", false);
                          ...
                        }
                        ...
                      }
    pattern-sinks:
      - patterns:
          - pattern: $FACTORY.newSAXParser();
    pattern-sanitizers:
      - by-side-effect: true
        pattern-either:
          - patterns:
              - pattern-either:
                  - pattern: >
                      $FACTORY.setFeature("http://apache.org/xml/features/disallow-doctype-decl",
                      true);
                  - pattern: >
                      $FACTORY.setFeature("http://xml.org/sax/features/external-general-entities",
                      false);

                      ...

                      $FACTORY.setFeature("http://xml.org/sax/features/external-parameter-entities", false);
                  - pattern: >
                      $FACTORY.setFeature("http://xml.org/sax/features/external-parameter-entities",
                      false);

                      ...

                      $FACTORY.setFeature("http://xml.org/sax/features/external-general-entities", false);
              - focus-metavariable: $FACTORY
          - patterns:
              - pattern-either:
                  - pattern-inside: >
                      class $C {
                        ...
                        $T $M(...) {
                          ...
                          $FACTORY.setFeature("http://apache.org/xml/features/disallow-doctype-decl",
                          true);
                          ...
                        }
                        ...
                      }
                  - pattern-inside: >
                      class $C {
                        ...
                        $T $M(...) {
                          ...
                          $FACTORY.setFeature("http://xml.org/sax/features/external-general-entities", false);
                          ...
                          $FACTORY.setFeature("http://xml.org/sax/features/external-parameter-entities", false);
                          ...
                        }
                        ...
                      }
                  - pattern-inside: >
                      class $C {
                        ...
                        $T $M(...) {
                          ...
                          $FACTORY.setFeature("http://xml.org/sax/features/external-parameter-entities", false);
                          ...
                          $FACTORY.setFeature("http://xml.org/sax/features/external-general-entities",false);
                          ...
                        }
                        ...
                      }
              - pattern: $M($X)
              - focus-metavariable: $X
