# yamllint disable
# License: Commons Clause License Condition v1.0[LGPL-2.1-only]
# https://semgrep.dev/r?q=java.lang.security.audit.xxe.documentbuilderfactory-external-parameter-entities-true.documentbuilderfactory-external-parameter-entities-true
# yamllint enable
---
rules:
  - id: java_xxe_rule-ExternalParameterEntitiesTrue
    severity: "WARNING"
    languages:
      - "java"
    metadata:
      cwe: "CWE-611"
      shortDescription: "Improper restriction of XML external entity reference"
      owasp:
        - "A4:2017-XML External Entities (XXE)"
        - "A05:2021-Security Misconfiguration"
      security-severity: "MEDIUM"
      references:
        - "https://semgrep.dev/blog/2022/xml-security-in-java"
        - "https://semgrep.dev/docs/cheat-sheets/java-xxe/"
        - "https://blog.sonarsource.com/secure-xml-processor"
      category: "security"
      technology:
        - "java"
        - "xml"
      cwe2022-top25: "true"
      cwe2021-top25: "true"
      license: "Commons Clause License Condition v1.0[LGPL-2.1-only]"
      vulnerability_class:
        - "XML Injection"
    message: |
      This rule identifies instances in the code where the XML parser is
      configured to allow external parameter entities. Enabling external
      entities in XML parsing can lead to XML External Entity (XXE) attacks,
      where an attacker could exploit the XML parser to read sensitive files
      from the server, perform denial of service (DoS) attacks, or achieve
      server-side request forgery (SSRF). This vulnerability occurs when the
      application processes XML input that includes external entity references
      within an XML document.

      To mitigate this vulnerability, the application should be configured to disable 
      the use of external entities in XML parsing. This can be achieved by setting the 
      feature "http://xml.org/sax/features/external-parameter-entities" to false.

      Secure Code Example: 
      ``` 
      try {
          DBFactory dbFactory = DocumentBuilderFactory.newInstance();
          // Disable external entities to mitigate XXE vulnerability
          dbFactory.setFeature("http://xml.org/sax/features/external-parameter-entities", false);
          dbFactory.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
          // Additional configuration for secure parsing
          dbFactory.setFeature("http://xml.org/sax/features/external-general-entities", false);
          dbFactory.setXIncludeAware(false);
          dbFactory.setExpandEntityReferences(false);

          // Use the configured factory to create a document builder
          DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
          // Parse an XML file using the secure document builder
          Document doc = dBuilder.parse(new InputSource(new StringReader(xmlData)));
          // Process the document as needed
      } catch (ParserConfigurationException | SAXException | IOException e) {
          // Handle exceptions appropriately
      } 
      ``` 
    pattern: |
      $PARSER.setFeature("http://xml.org/sax/features/external-parameter-entities",
      true);
