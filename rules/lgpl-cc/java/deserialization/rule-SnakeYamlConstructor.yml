# yamllint disable
# License: Commons Clause License Condition v1.0[LGPL-2.1-only]
# https://github.com/semgrep/semgrep-rules/blob/release/java/lang/security/use-snakeyaml-constructor.yaml
# yamllint enable
---
rules:
  - id: java_deserialization_rule-SnakeYamlConstructor
    languages:
      - java
    patterns:
      - pattern: |
          $Y = new org.yaml.snakeyaml.Yaml();
          ...
          $Y.load(...);
    message: |
      The application uses SnakeYAML org.yaml.snakeyaml.Yaml() constructor 
      with no arguments, which is vulnerable to deserialization attacks. 
      When Yaml() (no-argument constructor) is used, SnakeYAML uses the 
      default Constructor which can instantiate any Java object during 
      deserialization. This is risky because an attacker could craft malicious 
      YAML files to execute arbitrary code or perform unauthorized actions.

      To mitigate the issue and to safely use SnakeYaml to parse YAML data, 
      always make sure to only ever use a Yaml instance that is constructed 
      either with a SafeConstructor or an instance constructed with a 
      Constructor specifying a specific class. 

      Please note, if you want to use Constructor with a specific target class
      as a mitigation criteria, ensure that snakeyaml version is 2.0 or higher
      as Constructor class is vulnerable in snakeyaml versions lower than 2.0.
      Refer - https://github.com/google/security-research/security/advisories/GHSA-mjmj-j48q-9wg2

      Secure Code Examples:
      ```
      public void safeConstructorLoad(String toLoad) {
        // Configure LoaderOptions for safe deserialization
        LoaderOptions loaderOptions = new LoaderOptions();
        loaderOptions.setAllowDuplicateKeys(false);
        loaderOptions.setMaxAliasesForCollections(50);

        Yaml yaml = new Yaml(new SafeConstructor(new LoaderOptipon()));
        yaml.load(toLoad);
      }

      public void customConstructorLoad(String toLoad, Class<?> goodClass) {
        // Use Constructor with a specific target class
        LoaderOptions loaderOptions = new LoaderOptions();
        Constructor customConstructor = new Constructor(goodClass, loaderOptions);

        Yaml yaml = new Yaml(customConstructor);
        yaml.load(toLoad);
      }
      ```
    metadata:
      cwe: "CWE-502"
      owasp:
        - "A8:2017-Insecure Deserialization"
        - "A08:2021-Software and Data Integrity Failures"
      category: "security"
      shortDescription: "Deserialization of untrusted data"
      security-severity: "HIGH"
      references:
        - "https://securitylab.github.com/research/swagger-yaml-parser-vulnerability/#snakeyaml-deserialization-vulnerability"
      technology:
        - "snakeyaml"
      likelihood: "LOW"
      impact: "HIGH"
      confidence: "LOW"
    severity: "ERROR"
