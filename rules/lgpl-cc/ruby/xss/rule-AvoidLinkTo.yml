# yamllint disable
# License: Commons Clause License Condition v1.0[LGPL-2.1-only]
# yamllint enable
---
rules:
- id: "ruby_xss_rule-AvoidLinkTo"
  mode: taint
  pattern-sources:
  - pattern: params
  - pattern: cookies
  - pattern: request.env
  - pattern-either:
    - pattern: $MODEL.url(...)
    - pattern: $MODEL.uri(...)
    - pattern: $MODEL.link(...)
    - pattern: $MODEL.page(...)
    - pattern: $MODEL.site(...)
  pattern-sinks:
  - pattern: link_to(...)
  pattern-sanitizers:
  - patterns:
    - pattern: |
        "...#{...}..."
    - pattern-not: |
        "#{...}..."
  message: |
    This code includes user input in `link_to`. In Rails, the body of 
    `link_to` is not escaped. This means that user input which reaches the 
    body will be executed when the HTML is rendered. Even in other versions, 
    values starting with `javascript:` or `data:` are not escaped. 

    Mitigation Strategy:
    Always sanitize user input used within `link_to` method calls. For versions of 
    Rails where `link_to` does not automatically escape the body, or when dealing 
    with schemes that are not escaped (`javascript:`, `data:`), manually escape or 
    validate the input against a list of safe values. Consider using the `sanitize` 
    helper method or other Rails sanitization helpers to clean user input before 
    rendering.

    Secure Code Example:
    ```
    user_input = params[:user_link_text]

    # This example uses the `sanitize` helper to ensure any HTML tags or JavaScript in the user input are escaped
    safe_link_text = sanitize(user_input)

    # Use the sanitized text as the body of the `link_to` method
    <%= link_to safe_link_text, some_safe_path %>
    ```
  languages: 
  - "ruby"
  severity: "WARNING"
  metadata:
    shortDescription: "Improper neutralization of input during web page generation
      ('Cross-site Scripting')"
    category: "security"
    cwe: "CWE-79"
    owasp:
    - "A7:2017-Cross-Site Scripting (XSS)"
    - "A03:2021-Injection"
    security-severity: "MEDIUM"
    technology:
    - "rails"
    references:
    - "https://brakemanscanner.org/docs/warning_types/link_to/"
    - "https://brakemanscanner.org/docs/warning_types/link_to_href/"
    - "https://github.com/semgrep/semgrep-rules/blob/develop/ruby/rails/security/audit/xss/avoid-link-to.yaml"

  