# yamllint disable
# License: Commons Clause License Condition v1.0[LGPL-2.1-only]
# yamllint enable
---
rules:
- id: ruby_crypto_rule-WeakHashesSHA1
  message: |
    The SHA-1 hashing algorithm is no longer considered secure for
    cryptographic applications due to its vulnerability to collision attacks,
    where two different inputs produce the same output hash. SHA-1's
    susceptibility to collision attacks undermines the security of
    cryptographic operations, allowing attackers to forge signatures or
    manipulate data without detection. This poses significant risks in
    authentication systems, data integrity validations, and secure
    communications. 

    Remediation: To mitigate this vulnerability, replace the SHA1 hashing 
    algorithm with stronger cryptographic hash functions, such as SHA-256 
    or SHA-3. These algorithms offer significantly improved security and 
    are resistant to collision attacks, making them suitable for cryptographic 
    purposes in modern applications.

    Secure Code example : 
    ``` 
    require 'openssl'

    data = "sensitive information"
    # Using SHA-256
    digest = OpenSSL::Digest::SHA256.new
    hash = digest.digest(data)
    hex_hash = digest.hexdigest(data)

    # Using SHA-3 (256 bits)
    digest = OpenSSL::Digest::SHA3.new(256)
    hash2 = digest.digest(data)
    hex_hash2 = digest.hexdigest(data)
    ```
  metadata:
    shortDescription: "Use of weak hash"
    category: "security"
    owasp:
    - "A3:2017-Sensitive Data Exposure"
    - "A02:2021-Cryptographic Failures"
    cwe: "CWE-328"
    security-severity: "MEDIUM"
    references:
    - "https://github.com/semgrep/semgrep-rules/blob/develop/ruby/lang/security/weak-hashes-sha1.yaml"
    - "https://security.googleblog.com/2017/02/announcing-first-sha1-collision.html"
    - "https://shattered.io/"
    technology:
    - "ruby"
  languages:
  - "ruby"
  severity: "WARNING"
  pattern-either:
  - pattern: Digest::SHA1.$FUNC
  - pattern: OpenSSL::Digest::SHA1.$FUNC
  - pattern: OpenSSL::HMAC.$FUNC("sha1",...)