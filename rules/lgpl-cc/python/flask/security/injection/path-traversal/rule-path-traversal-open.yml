# yamllint disable
# License: Commons Clause License Condition v1.0 [LGPL-2.1-only]
# source: https://github.com/semgrep/semgrep-rules/blob/release/python/flask/security/injection/path-traversal-open.yaml
# yamllint enable
---
rules:
- id: "python_flask_rule-path-traversal-open"
  languages:
  - "python"
  severity: "ERROR"
  message: |
    Found request data in a call to 'open'. An attacker can manipulate this input to access files outside the intended 
    directory. This can lead to unauthorized access to sensitive files or directories. To prevent path traversal attacks, 
    avoid using user-controlled input in file paths. If you must use user-controlled input, validate and sanitize the 
    input to ensure it does not contain any path traversal sequences. For example, you can use the `os.path.join` function 
    to safely construct file paths or validate that the absolute path starts with the directory which is whitelisted for 
    accessing file. The following code snippet demonstrates how to validate a file path from user-controlled input:
    ```
    import os

    def safe_open_file(filename, base_path):
      # Resolve the absolute path of the user-supplied filename
      absolute_path = os.path.abspath(filename)

      # Check that the absolute path starts with the base path
      if not absolute_path.startswith(base_path):
          raise ValueError("Invalid file path")

      return open(absolute_path, 'r')
    ```
    For more information, see the OWASP Path Traversal page: https://owasp.org/www-community/attacks/Path_Traversal
  metadata:
    cwe: "CWE-22"
    shortDescription: "Improper Limitation of a Pathname to a Restricted Directory ('Path Traversal')"
    owasp:
    - "A5:2017-Broken Access Control"
    - "A01:2021-Broken Access Control"
    category: "security"
    security-severity: "CRITICAL"
    references:
    - https://owasp.org/www-community/attacks/Path_Traversal
    technology:
    - "flask"
    likelihood: "MEDIUM"
    impact: "HIGH"
    confidence: "MEDIUM"
  pattern-either:
  - patterns:
    - pattern: open(...)
    - pattern-either:
      - pattern-inside: |
          @$APP.route($ROUTE, ...)
          def $FUNC(..., $ROUTEVAR, ...):
            ...
            open(..., <... $ROUTEVAR ...>, ...)
      - pattern-inside: |
          @$APP.route($ROUTE, ...)
          def $FUNC(..., $ROUTEVAR, ...):
            ...
            with open(..., <... $ROUTEVAR ...>, ...) as $FD:
              ...
      - pattern-inside: |
          @$APP.route($ROUTE, ...)
          def $FUNC(..., $ROUTEVAR, ...):
            ...
            $INTERIM = <... $ROUTEVAR ...>
            ...
            open(..., <... $INTERIM ...>, ...)
  - pattern: open(..., <... flask.request.$W.get(...) ...>, ...)
  - pattern: open(..., <... flask.request.$W[...] ...>, ...)
  - pattern: open(..., <... flask.request.$W(...) ...>, ...)
  - pattern: open(..., <... flask.request.$W ...>, ...)
  - patterns:
    - pattern-inside: |
        $INTERIM = <... flask.request.$W.get(...) ...>
        ...
        open(<... $INTERIM ...>, ...)
    - pattern: open(...)
  - patterns:
    - pattern-inside: |
        $INTERIM = <... flask.request.$W[...] ...>
        ...
        open(<... $INTERIM ...>, ...)
    - pattern: open(...)
  - patterns:
    - pattern-inside: |
        $INTERIM = <... flask.request.$W(...) ...>
        ...
        open(<... $INTERIM ...>, ...)
    - pattern: open(...)
  - patterns:
    - pattern-inside: |
        $INTERIM = <... flask.request.$W ...>
        ...
        open(<... $INTERIM ...>, ...)
    - pattern: open(...)
  - patterns:
    - pattern-inside: |
        $INTERIM = <... flask.request.$W.get(...) ...>
        ...
        with open(<... $INTERIM ...>, ...) as $F:
          ...
    - pattern: open(...)
  - patterns:
    - pattern-inside: |
        $INTERIM = <... flask.request.$W[...] ...>
        ...
        with open(<... $INTERIM ...>, ...) as $F:
          ...
    - pattern: open(...)
  - patterns:
    - pattern-inside: |
        $INTERIM = <... flask.request.$W(...) ...>
        ...
        with open(<... $INTERIM ...>, ...) as $F:
          ...
    - pattern: open(...)
  - patterns:
    - pattern-inside: |
        $INTERIM = <... flask.request.$W ...>
        ...
        with open(<... $INTERIM ...>, ...) as $F:
          ...
    - pattern: open(...)