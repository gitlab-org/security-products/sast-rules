# yamllint disable
# License: Commons Clause License Condition v1.0[LGPL-2.1-only]
# source: https://github.com/semgrep/semgrep-rules/blob/release/python/flask/security/open-redirect.yaml
# yamllint enable
---
rules:
  - id: "python_flask_rule-flask-open-redirect"
    languages:
      - "python"
    patterns:
      - pattern-inside: |
          @$APP.route(...)
          def $X(...):
            ...
      - pattern-not-inside: |
          @$APP.route(...)
          def $X(...):
            ...
            if <... werkzeug.urls.url_parse($V) ...>:
              ...
      - pattern-either:
          - pattern: "flask.redirect(<... flask.request.$W.get(...) ...>, ...)"
          - pattern: "flask.redirect(<... flask.request.$W[...] ...>, ...)"
          - pattern: "flask.redirect(<... flask.request.$W(...) ...>, ...)"
          - pattern: "flask.redirect(<... flask.request.$W ...>, ...)"
          - pattern: |
              $V = flask.request.$W.get(...)
              ...
              flask.redirect(<... $V ...>, ...)
          - pattern: |
              $V = flask.request.$W[...]
              ...
              flask.redirect(<... $V ...>, ...)
          - pattern: |
              $V = flask.request.$W(...)
              ...
              flask.redirect(<... $V ...>, ...)
          - pattern: |
              $V = flask.request.$W
              ...
              flask.redirect(<... $V ...>, ...)
      - pattern-not: "flask.redirect(flask.request.path)"
      - pattern-not: "flask.redirect(flask.request.path + ...)"
      - pattern-not: "flask.redirect(f\"{flask.request.path}...\")"
    message: >-
      Data from request is passed to redirect(). This is an open redirect and could be exploited. Consider using 'url_for()' 
      to generate links to known locations. If you must use a URL to unknown pages, consider using 'urlparse()' or similar 
      and checking if the 'netloc' property is the same as your site's host name. For example:
      
        Example:
        ```python
        from flask import Flask, request, redirect, url_for
        from urllib.parse import urlparse
      
        app = Flask(__name__)
      
        @app.route('/login')
        def login():
            next = request.args.get('next')
            if not next:
                next = url_for('index')
            if urlparse(next).netloc != urlparse(request.url).netloc:
                return redirect(url_for('index'))
            return redirect(next)
        ```
       See the references for more information.
    metadata:
      cwe: "CWE-601"
      shortDescription: "URL redirection to untrusted site ('Open Redirect')"
      owasp:
        - "A01:2021-Broken Access Control"
        - "A5:2017-Broken Access Control"
      category: "security"
      security-severity: "MEDIUM"
      references: |
        - https://flask-login.readthedocs.io/en/latest/#login-example
        - https://cheatsheetseries.owasp.org/cheatsheets/Unvalidated_Redirects_and_Forwards_Cheat_Sheet.html#dangerous-url-redirect-example-1
        - https://docs.python.org/3/library/urllib.parse.html#url-parsing
      technology:
        - "flask"
      subcategory:
        - "audit"
      likelihood: "LOW"
      impact: "MEDIUM"
      confidence: "LOW"
    severity: "WARNING"